import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/*
 * java.util.concurrent.ThreadFactory を使うと
 * Threadの生成部分を隠蔽することができる。
 */

/**
 * Main
 */
public class Main {
	public static void main(String[] args) {
		ThreadFactory factory = Executors.defaultThreadFactory();
		factory.newThread(new Printer("Nice!")).start();
		for (int i = 0; i < 10000; i++) {
			System.out.print("Good!");
		}
	}
}
