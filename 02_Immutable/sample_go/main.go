package main

import (
	"fmt"
	"time"
)

type Person struct {
	name    string
	address string
}

func (p Person) String() string {
	return fmt.Sprintf("[ Persion: name = %s, address = %s ]", p.name, p.address)
}

type PrintPersonThread struct {
	id     int
	person Person
}

func (t PrintPersonThread) Start() {
	for {
		fmt.Printf("No. %d thread prints %s\n", t.id, t.person)
		time.Sleep(1 * time.Second)
	}
}

func main() {
	alice := Person{name: "Alice", address: "Alaska"}

	go PrintPersonThread{id: 0, person: alice}.Start()
	go PrintPersonThread{id: 1, person: alice}.Start()
	go PrintPersonThread{id: 2, person: alice}.Start()

	select {}
}

/*
   考察

   golangにはjavaのfinalに相当する機能がないので、Immutabilityはプログラマが
   確保する必要がある

   フィールド名の最初の一文字を小文字にすれば、package外からのアクセスができなく
   なるので、それを利用する

*/
