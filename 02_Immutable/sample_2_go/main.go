package main

import (
	"./person"
)

func main() {
	alice := person.Person("Alice", "Alaska")

	go person.PrintPersonThread(0, alice).Start()
	go person.PrintPersonThread(1, alice).Start()
	go person.PrintPersonThread(2, alice).Start()

	select {}
}

/*
   考察

   golangにはjavaのfinalに相当する機能がないので、Immutabilityはプログラマが
   確保する必要がある(この場合はpersonパッケージの作成者)

   personパッケージを別途作り、mainからはperson型、printPersonThread型に直接
   触れないようにしている(エクスポートしていない)

   エクスポートしていない型のデータを作成するためにfactoryを作った


   この場合、person.person型の変数宣言で以下の形式が使えない
   (person型がエクスポートされていないので)
   //var bob person.person				// ここでコンパイルエラーする
   //bob = person.Person("Bob", "Britain")

   := を使って直接受けるようにする

*/
