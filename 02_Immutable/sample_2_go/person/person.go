package person

import (
	"fmt"
	"time"
)

type person struct {
	name    string
	address string
}

/* factory */
func Person(name, address string) person {
	return person{name: name, address: address}
}

func (p person) String() string {
	return fmt.Sprintf("[ Persion: name = %s, address = %s ]", p.name, p.address)
}

type printPersonThread struct {
	id     int
	person person
}

/* factory */
func PrintPersonThread(id int, p person) printPersonThread {
	return printPersonThread{id: id, person: p}
}

func (t printPersonThread) Start() {
	for {
		fmt.Printf("No. %d thread prints %s\n", t.id, t.person)
		time.Sleep(1 * time.Second)
	}
}

/*
   考察

   personパッケージを作り、構造体をエクスポートしないようにした
   後はこのパッケージ内で構造体内の変数を書き換えるようなコードがなければ
   不変性は確保されることになる
*/
