import java.util.List;

/**
 * WriterThread
 */
public class WriterThread extends Thread {
	private final List<Integer> list;
	/**
	 * Constructor
	 */
	public WriterThread(List<Integer> list) {
		super("WriterThread");
		this.list = list;
	}
	public void run() {
		for (int i = 0; true; i++) {
			list.add(i);
			list.remove(0);		// 先頭要素を取り除く
		}
	}
}
