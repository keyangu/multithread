import java.util.List;

/**
 * ReaderThread
 */
public class ReaderThread extends Thread {
	private final List<Integer> list;
	/**
	 * Constructor
	 */
	public ReaderThread(List<Integer> list) {
		super("ReaderThread");
		this.list = list;
	}
	public void run() {
		while (true) {
			synchronized (list) {
				for (int n : list) {		// J2SE5.0 からの拡張forループ
					System.out.println(n);
				}
			}
		}
	}
}
