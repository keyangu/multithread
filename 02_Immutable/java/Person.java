/**
 * Person
 */
public final class Person {
	private final String name;
	private final String address;
	/**
	 * Constructor
	 */
	public Person(String name, String address) {
		this.name = name;
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public String toString() {
		return "[ Person: name = " + name + ", address = " + address + " ]";
	}
}

/*
 * Immutable役
 * - 外部からアクセス可能なフィールドが全てimmutableになっている
 * - 外部から直接フィールドにアクセス出来ない
 * - フィールドの内容を変更するようなメソッドがない
 *
 * このオブジェクトを扱う場合は並行処理をしても同期を気にする必要がない
 *
 * 使いどころ
 * - インスタンスの生成後、状態が変わらないとき
 * - インスタンスが共有され、頻繁にアクセスされるとき
 *
 * mutableクラスとimmutableクラスの対を考えてみる
 * - mutableクラスの中でimmutableにできるところを抜き出して、immutableクラスにする
 * - 標準クラスライブラリにも対になっているものが存在する
 *
 * 不変性は継承やちょっとしたコード変更(mutableなフィールドを持ったり)で壊れやすい
 * 不変性が失われると、それを利用しているスレッドの安全性も失われるので、
 * 変更時に注意するようにドキュメントなどには記載したほうがよい
 *
 * 他のオブジェクトをフィールドで持つようなクラスだと、finalにしても、そのインスタンスの
 * 中身自体が変更可能であればmutableなので気づきにくい
 *
 *
 * 関連パターン
 * - Single Threaded Execution
 *   - インスタンスの状態が変化する場合はこちら
 * - Read-Write Lock
 *   - read-readではコンフリクトが起きないことを利用してこちらの場合も
 * - Flyweight
 *   - メモリの利用効率を上げるためにインスタンスの共有を行う(これはマルチスレッドじゃない方の
 *     デザインパターン)
 *
 *
 * 補講1
 * javaのfinalについて
 * - finalクラス: クラス宣言にfinalが付いていると、そのクラスは拡張できない
 * - finalメソッド: インスタンスメソッドの宣言にfinalが付いていると、サブクラスでオーバーライドできない
 *     クラスメソッドの宣言にfinalが付いていると、サブクラスのメソッドで隠蔽できない
 * - finalフィールド: 1度しか代入できない。インスタンスフィールドへの代入は宣言時かコンストラクタ内
 *     クラスフィールドの場合は、宣言時かstaticブロックの中でフィールドへ代入
 * - final変数、引数: final変数も代入は1度だけ。final引数はメソッド呼び出し時に値が入るので、それ以降の代入は不可
 */


