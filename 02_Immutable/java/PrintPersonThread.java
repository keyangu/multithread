/**
 * PrintPersonThread
 */
public class PrintPersonThread extends Thread {
	private Person person;
	/**
	 * Constructor
	 */
	public PrintPersonThread(Person person) {
		this.person = person;
	}
	public void run() {
		while (true) {
			System.out.println(Thread.currentThread().getName() + " prints " + person);
		}
	}
}
