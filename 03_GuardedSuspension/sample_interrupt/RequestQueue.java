import java.util.Queue;
import java.util.LinkedList;

/**
 * RequestQueue
 */
public class RequestQueue {
	private final Queue<Request> queue = new LinkedList<Request>();

	// synchronized は queue.peek() -> queue.remove() をクリティカルセクションにするため
	public synchronized Request getRequest() throws InterruptedException {
		// ガード条件(queue.peek() != null)
		// whileを抜ける時が条件を満たす時なので、while内は論理否定
		while (queue.peek() == null) {
			// waitに入るとスレッドはthisのウェイトセットに入り、ロックが開放される
			wait();
		}
		// queue.peek() != null が保証されているので、NoSuchElementExceptionは投げられることはない
		// ある処理の前に必ず満たされている条件のことを事前条件(precondition)と呼ぶ。
		return queue.remove();
	}

	public synchronized void putRequest(Request request) {
		queue.offer(request);
		System.out.println("before notifyAll");
		notifyAll();
		System.out.println("after notifyAll");
	}
}


/*
 * Guarded Suspension の登場人物
 *
 * GuardedObject(ガードされているオブジェクト)役
 *  GuardedObject役はguardedMethod(ガードされたメソッド)を持っているクラス。
 *  ガード条件の真偽はGuardedObject役の状態に応じて変化する
 *
 *  また、GuardedObject役はインスタンスの状態を変化させるstateChangingMethodを持つ
 *  場合がある。
 *
 *  JavaではwhileとwaitでguardedMethodを実現し、
 *  notify/notifyAllでstateChangingMethodを実現する
 *
 *
 * 発展のヒント
 * ・条件付きのsynchronized
 *   スレッドが待たされるかどうかはガード条件によって決まる。
 *
 * ・マルチスレッド版のif
 *   このパターンはマルチスレッド版のifのようなもの。
 *   シングルスレッドの場合は単なるif文。
 *
 * ・状態変更忘れと生存性
 *   notifyで起こしてもガード条件が満たされるように状態変更するのを忘れずに！
 *   wait()する側もタイムアウトで待って、必要なくなったらやめるという処理も書ける(Balkingパターン)
 *
 * ・wait, notify/notifyAllの責任(再利用性)
 *   waitやnotifyAllをRequestQueueの中に閉じ込めることで、他のクラスは意識せずに使えている
 *
 * ・似たようなパターンの呼び名
 *  - guarded suspension
 *    「ガードされて実行を一時中断する。」実装方法は表現されていない
 *  - guarded wait
 *    「ガードされて待つ。」wait/notifyを意識した名前
 *  - busy wait
 *    「忙しく待つ。」yieldしながら条件を待つ。Thread.yield()で実行可能
 *  - spin lock
 *    「回転してロックする。」ループで条件を確認することに注目した表現
 *    guarded waitでもbusy waitでもspin lockは当てはまる
 *  - polling
 *    「世論調査をする。」あるイベントが起きるのを繰り返し調べに行き、起こったら処理する
 */
