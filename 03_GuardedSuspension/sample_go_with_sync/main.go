package main

import (
	"container/list"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type Request struct {
	name string
}

func (r *Request) String() string {
	return fmt.Sprintf("[ Reqeust %s ]", r.name)
}

type RequestQueue struct {
	queue *list.List
	cond  *sync.Cond
}

func (rq *RequestQueue) init() {
	rq.queue = list.New()
	mutex := new(sync.Mutex)
	rq.cond = sync.NewCond(mutex)
}

func (rq *RequestQueue) GetRequest() Request {
	rq.cond.L.Lock()
	defer rq.cond.L.Unlock()
	for rq.queue.Len() <= 0 {
		// wait
		rq.cond.Wait()
	}

	// キューから要素を取り出して削除
	element := rq.queue.Front()
	request := element.Value.(Request)
	rq.queue.Remove(element)

	return request
}

func (rq *RequestQueue) PutRequest(request Request) {
	rq.cond.L.Lock()
	defer rq.cond.L.Unlock()
	rq.queue.PushBack(request)
	// notifyに相当する作業
	rq.cond.Signal()
}

type ClientThread struct {
	name         string
	random       *rand.Rand
	requestQueue *RequestQueue
}

func NewClientThread(requestQueue *RequestQueue, name string, seed int64) *ClientThread {
	r := rand.New(rand.NewSource(seed))
	return &ClientThread{name, r, requestQueue}
}

func (ct *ClientThread) Start(c chan int) {
	for i := 0; i < 1000; i++ {
		request := Request{name: fmt.Sprintf("No.%d", i)}
		fmt.Printf("%s requests %s\n", ct, request)
		ct.requestQueue.PutRequest(request)
		time.Sleep(time.Duration(ct.random.Intn(1000)) * time.Millisecond)
	}

	c <- 1
}

func (ct *ClientThread) String() string {
	return fmt.Sprintf("ClientThread: %v", ct.name)
}

type ServerThread struct {
	name         string
	random       *rand.Rand
	requestQueue *RequestQueue
}

func NewServerThread(requestQueue *RequestQueue, name string, seed int64) *ServerThread {
	r := rand.New(rand.NewSource(seed))
	return &ServerThread{name, r, requestQueue}
}

func (st *ServerThread) Start(c chan int) {
	for i := 0; i < 10000; i++ {
		request := st.requestQueue.GetRequest()
		fmt.Printf("%s handles %s\n", st, request)
		time.Sleep(time.Duration(st.random.Intn(1000)) * time.Millisecond)
	}

	c <- 2
}

func (st ServerThread) String() string {
	return fmt.Sprintf("ServerThread: %v", st.name)
}

func main() {
	var requestQueue RequestQueue
	requestQueue.init()

	c := make(chan int, 2)

	go NewClientThread(&requestQueue, "Alice", 3141592).Start(c)
	go NewServerThread(&requestQueue, "Bobby", 6535897).Start(c)

	<-c
	<-c
}
