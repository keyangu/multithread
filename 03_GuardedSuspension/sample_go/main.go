package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Request struct {
	name string
}

func (r Request) String() string {
	return fmt.Sprintf("[ Request %s ]", r.name)
}

func ClientThreadStart(seed int64, cr chan Request, cf chan int) {
	random := rand.New(rand.NewSource(seed))
	for i := 0; i < 10000; i++ {
		request := Request{name: fmt.Sprintf("No.%d", i)}
		fmt.Printf("Client requests %s\n", request)
		cr <- request
		time.Sleep(time.Duration(random.Intn(1000)) * time.Millisecond)
	}
}

func ServerThreadStart(seed int64, cr chan Request, cf chan int) {
	random := rand.New(rand.NewSource(seed))
	for i := 0; i < 10000; i++ {
		request := <-cr
		fmt.Printf("Server handles %s\n", request)
		time.Sleep(time.Duration(random.Intn(1000)) * time.Millisecond)
	}
}

func main() {
	cr := make(chan Request, 10)
	cf := make(chan int, 2)

	go ClientThreadStart(3141592, cr, cf)
	go ServerThreadStart(6535897, cr, cf)

	<-cf
	<-cf
}
