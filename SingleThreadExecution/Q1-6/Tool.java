/**
 * Tool
 */
public class Tool {
	private final String name;
	/**
	 * Constructor
	 */
	public Tool(String name) {
		this.name = name;
	}

	public String toString() {
		return "[ " + name + " ]";
	}
}
