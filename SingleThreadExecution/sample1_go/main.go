package main

import (
	"fmt"
	"strconv"
	"time"
)

type User struct {
	name    string
	address string
}

func (u *User) start(c chan *User) {
	fmt.Println(u.name + " BEGIN")
	for {
		c <- u
	}
}

type Gate struct {
	counter int
	user    *User
}

func (g *Gate) pass(c chan *User) {
	for u := range c {
		g.counter++
		g.user = u
		g.check()
	}
}

func (g *Gate) String() string {
	return "No." + strconv.Itoa(g.counter) + ": " + g.user.name + ", " + g.user.address
}

func (g *Gate) check() {
	if g.user.name[0] != g.user.address[0] {
		fmt.Println("***** BROKEN ***** ", g)
	}
	fmt.Printf("counter: %s\n", g)
}

func main() {
	fmt.Println("Testing Gate, hit CTRL+C to exit.")
	gate := &Gate{0, &User{"Nobody", "Nowhere"}}
	alice := &User{"Alice", "Alaska"}
	bobby := &User{"Bobby", "Brazil"}
	chris := &User{"Chris", "Canada"}

	c := make(chan *User)

	go gate.pass(c)
	go alice.start(c)
	go bobby.start(c)
	go chris.start(c)

	for {
		time.Sleep(time.Second)
	}
}
