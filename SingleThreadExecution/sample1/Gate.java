/**
 * Gate
 */
public class Gate {
	private int counter = 0;
	private String name = "Nobody";
	private String address = "Nowhere";

	public synchronized void pass(String name, String address) {
		this.counter++;
		this.name = name;
		this.address = address;
		check();
	}

	public synchronized String toString() {
		return "No." + counter + ": " + name + ", " + address;
	}

	private void check() {
		if (name.charAt(0) != address.charAt(0)) {
			System.out.println("***** BROKEN ***** " + this.toString());
		}
	}
}

/*
 * 解説
 *
 * Gate クラスが SharedResource 役
 * SharedResource役のメソッドは2種類
 *  safeMethod   -- 複数スレッドから同時に呼び出されても大丈夫
 *  unsafeMethod -- 複数スレッドから呼び出されるときにガードが必要なメソッド
 *
 * シングルスレッドで動かさなければならない範囲を「クリティカルセクション」という
 *
 * synchronizedブロックは {} の範囲でロック取得、解放を行う。
 * 明示的にlock()やunlock()がある仕組みの場合、中でreturnしたり、例外を吐いたりすると
 * unlock()されずに外に出る可能性があるが、synchronizedブロックの場合はその心配がない。
 * ---
 * lock();
 * try {
 *		なんか処理
 * } finally {
 *		unlock();
 * }
 * ---
 *  finallyはtry内のreturnや例外を拾って後処理してくれるので、
 *  これならsynchronizedブロックと透過。
 *
 *
 * 補講
 *
 * ・考え方のポイント
 *   - このsynchronizedは何を守っているのか？
 *     - 他の場所でもちゃんと守られているのか？
 *   - どの単位で守るべきか (Gateでは counter, name, addressはセットで守る)
 *   - どのロックを使って守っているのか (thisでロックをかけている)
 *
 * ・アトミックな操作
 *   - 基本型、参照型の代入や参照はアトミック
 *   - longやdoubleの代入や参照はアトミックではない
 *   - volatileで宣言するとアトミックになる(volatileの機能は他にもある)
 *   - synchronizedで囲ったブロックはアトミック
 *   - java.util.concurrent.atomic パッケージにアトミック操作補助ライブラリがある
 */
