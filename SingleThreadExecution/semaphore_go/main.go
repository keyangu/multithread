package main

import (
	"fmt"
	"math/rand"
	"time"
)

func doUse(id, cid int) {
	fmt.Printf("BEGIN: used = %v, %v\n", id, cid)
	time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
	fmt.Printf("END:   used = %v, %v\n", id, cid)
}

func UserRun(id int, r *rand.Rand, c chan int) {
	for {
		tc := <-c
		fmt.Printf("c: %v\n", len(c))
		doUse(id, tc)
		c <- tc
		time.Sleep(time.Duration(r.Intn(3000)) * time.Millisecond)
	}
}

func main() {
	c := make(chan int, 3)

	c <- 1
	c <- 2
	c <- 3

	for i := 0; i < 10; i++ {
		r := rand.New(rand.NewSource(26535))
		go UserRun(i, r, c)
	}

	for {
		time.Sleep(time.Second)
	}
}
